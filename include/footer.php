<!DOCTYPE html>
<html lang="en">
<head>
  	<title>Atomic Project</title>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	
  	<link rel="stylesheet" href="css/bootstrap.min.css">
  	<link rel="stylesheet" href="css/font-awesome.min.css">
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
  	
  	<link rel="stylesheet" href="css/jquery.dataTables.min.css">
	<script src="js/jquery.dataTables.min.js"></script>

  	<style>
  		footer{
                    background-color: #47b4b4;
			bottom: 0;
                        color: #fbfaf7;
			height: 50px;
			padding-top: 15px;
			text-align: center;
			width: 100%;
			margin-top: 30px;
		  }
		  
		  footer a{
			font-weight: bold;
                        color:  #c9194e;
		  }
		  
		  

		
  	</style>
  	
  	
</head>
<body>
    

<footer class="footer">
	<p>Designed &amp; developed by <a href="http://www.facebook.com/lejon.khan" target="_blank">Lejon Khan</a></p>
</footer>
</body>
</html>