<!DOCTYPE html>
<html lang="en">
<head>
  	<title>Atomic Project</title>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	
  	<link rel="stylesheet" href="css/bootstrap.min.css">
  	<link rel="stylesheet" href="css/font-awesome.min.css">
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
  	
  	<link rel="stylesheet" href="css/jquery.dataTables.min.css">
	<script src="js/jquery.dataTables.min.js"></script>

<!--  	<style>
  		footer{
                    background-color: #47b4b4;
			bottom: 0;
                        color: #fbfaf7;
			height: 50px;
			padding-top: 15px;
			text-align: center;
			width: 100%;
			margin-top: 30px;
		  }
		  
		  footer a{
			font-weight: bold;
                        color:  #c9194e;
		  }
		  
		  

		
  	</style>-->
  	
  	
</head>
<body>
    <div class="container-fluid bg-1 text-center" style="background-color: blueviolet;">
        <div class="text-center" style="color: white;">
		<h1>Atomic Project</h1>
		<br/>
                <h2>MD LEJON KHAN</h2>
		<h4>SEID:110455</h4>
                
  </div>
    </div>
<nav class="navbar navbar-inverse" style="background-color: #7e9494; height: 50px;">
  <div class="container-fluid">
      <div class="navbar-header text-center" >
          <a style="color: white" class="navbar-brand" href="index.php"><h3>Projects</h3></a>
     
      
    </div>
  </div>
</nav>
    


<div class="container">
	
   
	<div class="row">
		<table class="table table-striped table table-bordered">
			<thead>
				<tr>
					<th>Serial No</th>
					<th>Project Name</th>
					
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>01</td>
                                        <td><button type="button" class="btn btn-default  btn-lg btn-block"><a href="views/SEIP110455/Book/" style="color: #528CE0;">Book: title</a></button></td>
					
				</tr>
				
				<tr>
					<td>02</td>
                                        <td><button type="button" class="btn btn-default btn-lg btn-block"><a href="views/SEIP110455/Date/" style="color: #528CE0;">Date: Birthday</a></button></td>
					
				</tr>
				
				<tr>
					<td>03</td>
                                        <td><button type="button" class="btn btn-default btn-lg btn-block"><a href="views/SEIP110455/Interest/" style="color: #528CE0;">Interest:Hobby</a></button></td>
					
				</tr>
				
				<tr>
					<td>04</td>
                                        <td><button type="button" class="btn btn-default btn-lg btn-block"><a href="views/SEIP110455/Profile/" style="color: #528CE0;">Profile:picture</a></button></td>
				
				</tr>
				
				<tr>
					<td>05</td>
                                        <td><button type="button" class="btn btn-default btn-lg btn-block"><a href="views/SEIP110455/Qualification/" style="color: #528CE0;">Qualification:Education</a></button></td>
					
				</tr>

				<tr>
					<td>06</td>
                                        <td><button type="button" class="btn btn-default btn-lg btn-block"><a href="views/SEIP110455/Region/" style="color: #528CE0;">Region:City</a></button></td>
					
				</tr>
				
				<tr>
					<td>07</td>
                                        <td><button type="button" class="btn btn-default btn-lg btn-block"><a href="views/SEIP110455/Subscription/" style="color: #528CE0;">Subscription:Email</a></button></td>
					
				</tr>
				
				<tr>
					<td>08</td>
                                        <td><button type="button" class="btn btn-default btn-lg btn-block"><a href="views/SEIP110455/Summary/" style="color: #528CE0;">Summary:Textarea</a></button></td>
					
				</tr>
				
				<tr>
					<td>09</td>
                                        <td><button type="button" class="btn btn-default btn-lg btn-block"><a href="views/SEIP110455/Terms/" style="color: #528CE0;">Terms:Conditions</a></button></td>
					
				</tr>
				
				
			</tbody>
		</table>
	</div>
</div>

<?php include './include/footer.php'; ?>
</body>
</html>