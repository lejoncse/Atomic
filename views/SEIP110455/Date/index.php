<!DOCTYPE html>
<html lang="en">
<head>
  <title>Birthday</title>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>




<!--  <div class="container-fluid bg-1 text-center">
    <h3>LEJON KHAN</h3>
    <h3>110455</h3>
    <h3>I want to be a web developer</h3>-->
<nav class="navbar navbar" style="background-color: #999999">
  <div class="container-fluid">
      <div class="navbar-header" style="background-color:  #666666">
          <a class="navbar-brand" href="../../../index.php" style="color:white;"><b>Project Lists</b></a>
    </div>
    <div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>
            <li class="active"><a href="Create.php">Create Birthday</a></li>
            <li><a href="Store.php">Store Birthday</a></li> 
            <li><a href="Edit.php">Edit Birthday</a></li> 
            <li><a href="Update.php">Update Birthday</a></li>
        <li><a href="delete.php">Delete Birthday</a></li> 
        
      </ul>
    </div>
  </div>
</nav>
<div class="row">
    <button type="button" class="btn btn-primary btn-lg btn-block">
        <?php
      
           
           function __autoload($className){
               $fileName = str_replace("\\", "/", $className);
               include_once "../../../".$fileName.".php";
           }
       
       use src\Bitm\SEIP110455\Date\Birthday;
       
       $title =  new Birthday();
       $title->index();
        ?>
    </button>
</div>
 

<table id="list" class="table table-striped table-bordered dt-responsive nowrap" width="100%" cellspacing="0">
			<thead>
				<tr>
					<th>Serial No</th>
					<th>Name</th>
                                        <th>Birthday</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Arif Hossain</td>
                                        <td>09/04/1993</td>
					<td>
                                               <a href="Store.php" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                               <a href="Edit.php" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                               <a href="Update.php" class="btn btn-info"><span class="glyphicon glyphicon-refresh"></span> Update</a>
                                               <a href="Store.php" class="btn btn-success"><span class="glyphicon glyphicon-th"></span> Store</a>
						<button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
					</td>
				</tr>
				
				<tr>
					<td>2</td>
					<td>Mahmudul Hasan</td>
                                        <td>10/12/1992</td>
					<td>
						 <a href="Store.php" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                               <a href="Edit.php" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                               <a href="Update.php" class="btn btn-info"><span class="glyphicon glyphicon-refresh"></span> Update</a>
                                               <a href="Store.php" class="btn btn-success"><span class="glyphicon glyphicon-th"></span> Store</a>
						<button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
					</td>
				</tr>
				
				<tr>
					<td>3</td>
					<td>Johiril Islam</td>
                                        <td>24/11/1991</td>
					<td>
						 <a href="Store.php" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                               <a href="Edit.php" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                               <a href="Update.php" class="btn btn-info"><span class="glyphicon glyphicon-refresh"></span> Update</a>
                                               <a href="Store.php" class="btn btn-success"><span class="glyphicon glyphicon-th"></span> Store</a>
						<button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
					</td>
				</tr>
				
				<tr>
					<td>4</td>
					<td>Ariful Islam</td>
                                        <td>5/09/1991</td>
					<td>
						 <a href="Store.php" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                               <a href="Edit.php" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                               <a href="Update.php" class="btn btn-info"><span class="glyphicon glyphicon-refresh"></span> Update</a>
                                               <a href="Store.php" class="btn btn-success"><span class="glyphicon glyphicon-th"></span> Store</a>
						<button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
					</td>
				</tr>
				
				<tr>
					<td>5</td>
					<td>Sayem Ahmed</td>
                                        <td>01/02/1992</td>
					<td>
						 <a href="Store.php" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                               <a href="Edit.php" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                               <a href="Update.php" class="btn btn-info"><span class="glyphicon glyphicon-refresh"></span> Update</a>
                                               <a href="Store.php" class="btn btn-success"><span class="glyphicon glyphicon-th"></span> Store</a>
						<button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
					</td>
				</tr>

				<tr>
					<td>6</td>
					<td>Mehedi Hasan</td>
                                        <td>01/04/1995</td>
					<td>
						 <a href="Store.php" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                               <a href="Edit.php" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                               <a href="Update.php" class="btn btn-info"><span class="glyphicon glyphicon-refresh"></span> Update</a>
                                               <a href="Store.php" class="btn btn-success"><span class="glyphicon glyphicon-th"></span> Store</a>
						<button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
					</td>
				</tr>
				
				<tr>
					<td>7</td>
					<td>Rahim</td>
                                        <td>01/01/1990</td>
					<td>
						 <a href="Store.php" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                               <a href="Edit.php" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                               <a href="Update.php" class="btn btn-info"><span class="glyphicon glyphicon-refresh"></span> Update</a>
                                               <a href="Store.php" class="btn btn-success"><span class="glyphicon glyphicon-th"></span> Store</a>
						<button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
					</td>
				</tr>

				<tr>
					<td>8</td>
					<td>Karim</td>
                                        <td>01/01/1991</td>
					<td>
						 <a href="Store.php" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                               <a href="Edit.php" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                               <a href="Update.php" class="btn btn-info"><span class="glyphicon glyphicon-refresh"></span> Update</a>
                                               <a href="Store.php" class="btn btn-success"><span class="glyphicon glyphicon-th"></span> Store</a>
						<button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
					</td>
				</tr>

				<tr>
					<td>9</td>
					<td>Sharif</td>
                                        <td>02/03/1990</td>
					<td>
						 <a href="Store.php" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                               <a href="Edit.php" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                               <a href="Update.php" class="btn btn-info"><span class="glyphicon glyphicon-refresh"></span> Update</a>
                                               <a href="Store.php" class="btn btn-success"><span class="glyphicon glyphicon-th"></span> Store</a>
						<button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
					</td>
				</tr>

				<tr>
					<td>10</td>
					<td>Abul</td>
                                        <td>05/06/1993</td>
					<td>
						 <a href="Store.php" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                               <a href="Edit.php" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                               <a href="Update.php" class="btn btn-info"><span class="glyphicon glyphicon-refresh"></span> Update</a>
                                               <a href="Store.php" class="btn btn-success"><span class="glyphicon glyphicon-th"></span> Store</a>
						<button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
					</td>
				</tr>

				
			</tbody>
		</table>


<?php include '../../../include/footer.php';  ?>

  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>