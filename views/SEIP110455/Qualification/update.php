<!DOCTYPE html>
<html lang="en">
<head>
  <title>Update Education level</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>



<nav class="navbar navbar" style="background-color: #999999">
  <div class="container-fluid">
      <div class="navbar-header" style="background-color:  #666666">
          <a class="navbar-brand" href="../../../index.php" style="color:white;"><b>Project Lists</b></a>
    </div>
    <div>
       <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>
            <li class="active"><a href="Create.php">Create Education level</a></li>
            <li><a href="Store.php">Store Education level</a></li> 
            <li><a href="Edit.php">Edit Education level</a></li> 
            <li><a href="Update.php">Update Education level</a></li>
        <li><a href="#">Delete Education level</a></li> 
        
      </ul>
    </div>
  </div>
</nav>
<div class="row">
    <button type="button" class="btn btn-primary btn-lg btn-block">
        <?php
      
           
           function __autoload($className){
               $fileName = str_replace("\\", "/", $className);
               include_once "../../../".$fileName.".php";
           }
       
        use src\Bitm\SEIP110455\Qualification\Education;
       
       $title =  new Education();
       $title->Update();
        ?>
    </button>
</div>
<div class="container">
	<h1 class="text-center">Update Education level</h1>
	<hr/>
    <div class="">
		<div class="form-group">
                    <div class="form-group">
				<label>Name: </label>
				<input type="text" name="name" id="name" class="form-control" value="Type Name">
			</div>

			<label>Choose Qualification:</label>
			   <div class="radio">
				  <label><input type="radio" value="">MSc</label>
				</div>
				<div class="radio">
				  <label><input type="radio" value="">BSc</label>
				</div>
				<div class="radio">
				  <label><input type="radio" value="">MBA</label>
				</div>
				<div class="radio">
				  <label><input type="radio" value="">BBA</label>
				</div>
				<div class="radio">
				  <label><input type="radio"  value="">HND-BBA</label>
				</div>
				<div class="radio">
				  <label><input type="radio" value="">MBBS</label>
				</div>
				<div class="radio">
				  <label><input type="radio" value="">MA</label>
				</div>
                        <div class="radio">
				  <label><input type="radio" value="">BA</label>
				</div>
                        <div class="radio">
				  <label><input type="radio" value="">HSC</label>
				</div>
			</div>
			
			<div class="form-group pull-center">
				<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-refresh"></span> Add Qualification</button>
			</div>
		</form>


	</div>
</div>


<?php include '../../../include/footer.php';  ?>


  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>