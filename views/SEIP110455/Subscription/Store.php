<!DOCTYPE html>
<html lang="en">
<head>
  <title>Store Email</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>



<nav class="navbar navbar" style="background-color: #999999">
  <div class="container-fluid">
      <div class="navbar-header" style="background-color:  #666666">
          <a class="navbar-brand" href="../../../index.php" style="color:white;"><b>Project Lists</b></a>
    </div>
    <div>
       <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Home</a></li>
            <li class="active"><a href="Create.php">Create Email</a></li>
            <li><a href="Store.php">Store Email</a></li> 
            <li><a href="Edit.php">Edit Email</a></li> 
            <li><a href="Update.php">Update Email</a></li>
        <li><a href="#">Delete Email</a></li> 
        
      </ul>
    </div>
  </div>
</nav>
<div class="row">
    <button type="button" class="btn btn-primary btn-lg btn-block">
        <?php
      
           
           function __autoload($className){
               $fileName = str_replace("\\", "/", $className);
               include_once "../../../".$fileName.".php";
           }
       
       use src\Bitm\SEIP110455\Subscription\Email;
       
       $title =  new Email();
       $title->Store();
        ?>
    </button>
</div>
<div class="container">
	<h1 class="text-center">Store email</h1>
	<hr/>
    
    <div class="">
		<form action="" method="POST" >
			<div class="form-group">
				<label><h2>Email:</h2> </label>
                                <label ><h1><a href="#" target="blank">arifcse11@gmail.com</a></h1> </label>
				
			</div>
                    
			<div class="form-group">
			<a href="edit.php" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</a>
			<button class="btn btn-danger" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
			
		</div>

		</form>
	</div>
</div>



<?php include '../../../include/footer.php';  ?>


  </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>

</html>